// #region Modules
const compression = require('compression');
const express = require('express')
const app = express();
const env = require('process').env
// #endregion

// #region Variables
const port = 9001
const url = env.NODE_ENV === 'production'
    ? 'http://localhost:' + port // TODO: prod url
    : 'http://localhost:' + port
const dist = __dirname + '/public/'
// #endregion

// #region Server
app.use(compression());
app.use(express.static(dist));

app.listen(port, function(){
    console.log(`Express Server listening on ${url}`);
});
// #endregion